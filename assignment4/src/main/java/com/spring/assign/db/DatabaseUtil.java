package com.spring.assign.db;

import java.sql.Connection;
import java.sql.DriverManager;

public class DatabaseUtil {
	
	private static Connection con=null;
	public static Connection getConnection()
	{
		if(con!=null)return con;
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			 con=DriverManager.getConnection("jdbc:oracle:thin:@//localhost:1521/XE","system","1234");
			System.out.println("connected");
			return con;
					
		}
		catch(Exception e) {return null;}
		
	}

}
