<!DOCTYPE html>
<html>
<head>
<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
</head>
<body>

<h2>HDFC Bangalore Branches</h2>

<table>
  <tr>
    <th>BranchID</th>
    <th>BranchName</th>
    <th>Contact</th>
  </tr>
  <tr>
    <td>HDFC01</td>
    <td>Electronic city phase1</td>
    <td>87777</td>
  </tr>
  <tr>
    <td>HDFC02</td>
    <td>Electronic city phase2</td>
    <td>877567</td>
  </tr>
  <tr>
    <td>HDFC03</td>
    <td>Silk Board</td>
    <td>876345</td>
  </tr>
  <tr>
    <td>HDFC04</td>
    <td>Marathahalli</td>
    <td>879567</td>
  </tr>
  <tr>
    <td>HDFC05</td>
    <td>Bellandur</td>
    <td>876563</td>
  </tr>
  <tr>
    <td>HDFC06</td>
    <td>Whitefield</td>
    <td>9875567</td>
  </tr>
  <tr>
    <td>HDFC07</td>
    <td>Indira Nagar</td>
    <td>88735</td>
  </tr>
  <tr>
    <td>HDFC08</td>
    <td>Koramangala</td>
    <td>988668</td>
  </tr>
  <tr>
    <td>HDFC09</td>
    <td>Majestic</td>
    <td>879077</td>
  </tr>
  <tr>
    <td>HDFC10</td>
    <td>Hebbal</td>
    <td>890765</td>
  </tr>
  
  
</table>

</body>
</html>