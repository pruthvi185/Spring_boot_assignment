package com.assignment5;

import java.util.StringTokenizer;

public class Employee {

	
	@Override
	public String toString() {
		return "Employee [empid=" + empid + ", empname=" + empname + ", empmail=" + empmail + ", emplocation="
				+ emplocation + "]";
	}
	private String empid;
	private String empname;
	private String empmail;
	private String emplocation;
	
	public Employee() {}
	
	public Employee(String empid) {
		
		StringTokenizer st = new StringTokenizer(empid, "|");
		
		this.setEmpid(st.nextToken());
		this.setEmpname(st.nextToken());
		this.setEmpmail(st.nextToken());
		this.setEmplocation(st.nextToken());
	

	}
	public String getEmpid() {
		return empid;
	}
	public void setEmpid(String empid) {
		this.empid = empid;
	}
	public String getEmpname() {
		return empname;
	}
	public void setEmpname(String empname) {
		this.empname = empname;
	}
	public String getEmpmail() {
		return empmail;
	}
	public void setEmpmail(String empmail) {
		this.empmail = empmail;
	}
	public String getEmplocation() {
		return emplocation;
	}
	public void setEmplocation(String emplocation) {
		this.emplocation = emplocation;
	}
	
	
	
}
