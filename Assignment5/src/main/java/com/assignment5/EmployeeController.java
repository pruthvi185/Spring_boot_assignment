package com.assignment5;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeController {

	@Autowired
	private EmployeeDetails employeedetails;
	
	
	@RequestMapping("/employees")
	public HashMap<String, Employee> getallemployees()
	{
		return employeedetails.getallemployees();
		
	}
	
	@RequestMapping("/employees/{id}")
	public Employee getemployee(@PathVariable String id)
	{
		return employeedetails.getemployee(id);
		
	}
	
	@RequestMapping(method = RequestMethod.POST, value="/employees")
	public void addEmployee(@RequestBody Employee emp) {
		
		System.out.println(emp);
		
		employeedetails.addEmployee(emp);
	}
	
	@RequestMapping(method = RequestMethod.PUT, value="/employees/{id}")
	public void updateEmployee(@RequestBody Employee emp, @PathVariable String id) {
		
		employeedetails.updateEmployee(id,emp);
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value="/employees/{id}")
	public void deleteEmployee(@PathVariable String id) {
		
		employeedetails.deleteEmployee(id);
	}
	
}
